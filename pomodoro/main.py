import enum

class Status(enum.Enum):
    PRE_SESSION = 0
    WORK = 1
    STOP = 2
    PAUSE = 3

class Pomodoro:
    def __init__(self):
        self.session = Session()

class Session:
    def __init__(self):
        self.intervals = []
        self.status = Status.PRE_SESSION
    
    def start(self):
        if self.status == Status.PRE_SESSION:
            self.status = Status.WORK
            self.intervals.append(Interval(self.status))
        else:
            self.status = Status.WORK
            self.intervals[-1].status = Status.WORK

    def stop(self):
        self.status = Status.STOP
        self.intervals.append(Interval(self.status))

    def pause(self):
        self.status = Status.PAUSE
        self.intervals[-1].status = Status.PAUSE

class Interval():
    def __init__(self, status):
        self.status = status
        self.runs = [Run(self.status)]

class Run():
    def __init__(self, status):
        self.status = status