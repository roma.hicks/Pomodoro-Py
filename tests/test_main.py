import unittest

import pomodoro

class TestPomodoro(unittest.TestCase):
    # Pomodoro class initialises with a Session.
    def test_init_pomodoro(self):
        pClass = pomodoro.Pomodoro()
        self.assertNotEqual(pClass.session, None)

class TestSession(unittest.TestCase):
    # When a Session is initialised.
    # The Session should start with PRE_SESSION status and no Intervals.
    def test_init_session(self):
        sClass = pomodoro.Session()
        self.assertEqual(len(sClass.intervals), 0)
        self.assertEqual(sClass.status, pomodoro.Status.PRE_SESSION)

    # If a Session is started and its status is PRE_SESSION, the Session status is WORK.
    # An Interval is created and its status is WORK
    def test_start_new_session(self):
        sClass = pomodoro.Session()
        sClass.start()
        self.assertEqual(len(sClass.intervals), 1)
        self.assertEqual(sClass.status, pomodoro.Status.WORK)
        self.assertEqual(type(sClass.intervals[-1]).__name__, "Interval")
        self.assertEqual(sClass.intervals[-1].status, pomodoro.Status.WORK)

    # If a Session is started and its status is *NOT* PRE_SESSION, the Session status is WORK.
    # An Interval is *NOT* created and its status is WORK
    def test_start_other_session(self):
        sClass = pomodoro.Session()
        sClass.start()
        sClass.pause()
        sClass.start()
        self.assertEqual(len(sClass.intervals), 1)
        self.assertEqual(sClass.status, pomodoro.Status.WORK)
        self.assertEqual(type(sClass.intervals[-1]).__name__, "Interval")
        self.assertEqual(sClass.intervals[-1].status, pomodoro.Status.WORK)

    # If a Session is started and its status is STOP, the Session status is WORK.
    # An Interval is created and its status is WORK
    def test_start_stop_session(self):
        sClass = pomodoro.Session()
        sClass.start()
        sClass.stop()
        sClass.start()
        self.assertEqual(len(sClass.intervals), 2)
        self.assertEqual(sClass.status, pomodoro.Status.WORK)
        self.assertEqual(type(sClass.intervals[-1]).__name__, "Interval")
        self.assertEqual(sClass.intervals[-1].status, pomodoro.Status.WORK)

    # If a Session is stopped and the Session status is STOP.
    # An Interval is created and its status is STOP.
    def test_stop_session(self):
        sClass = pomodoro.Session()
        sClass.start()
        sClass.stop()
        self.assertEqual(len(sClass.intervals), 2)
        self.assertEqual(sClass.status, pomodoro.Status.STOP)
        self.assertEqual(type(sClass.intervals[-1]).__name__, "Interval")
        self.assertEqual(sClass.intervals[-1].status, pomodoro.Status.STOP)

    # If a Session is paused, the Session status is PAUSE.
    # An Interval is *NOT* created and its status is PAUSE.
    def test_pause_session(self):
        sClass = pomodoro.Session()
        sClass.start()
        sClass.pause()
        self.assertEqual(len(sClass.intervals), 1)
        self.assertEqual(sClass.status, pomodoro.Status.PAUSE)
        self.assertEqual(type(sClass.intervals[-1]).__name__, "Interval")
        self.assertEqual(sClass.intervals[-1].status, pomodoro.Status.PAUSE)

class TestInterval(unittest.TestCase):
    # Intervals created with status are of such status.
    # The initisation of the Interval creates a Run.
    def test_init_interval(self):
        iClass = pomodoro.Interval(pomodoro.Status.WORK)
        self.assertEqual(iClass.status, pomodoro.Status.WORK)
        self.assertEqual(type(iClass.runs[0]).__name__, "Run")
        self.assertEqual(len(iClass.runs), 1)

class TestRun(unittest.TestCase):
    # Runs created with status are of such status.
    def test_init_run(self):
        rClass = pomodoro.Run(pomodoro.Status.WORK)
        self.assertEqual(rClass.status, pomodoro.Status.WORK)
    
    # If a Session is started, the first run is a WORK run.
    def test_start_session(self):
        sClass = pomodoro.Session()
        sClass.start()
        self.assertEqual(len(sClass.intervals), 1)
        self.assertEqual(len(sClass.intervals[0].runs), 1)
        self.assertEqual(sClass.intervals[0].runs[0].status, pomodoro.Status.WORK)